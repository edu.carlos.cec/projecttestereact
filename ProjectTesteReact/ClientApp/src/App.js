import React, { Component } from 'react';
import { Route } from 'react-router';
import { Layout } from './components/Layout';
import { Home } from './components/Home';
import { FetchData } from './components/FetchData';
import { Counter } from './components/Counter';

import './custom.css'
import { Itinerario } from './components/Itinerario';
import { AtuacaoProfissional } from './components/AtuacaoProfissional';
import { Projetos } from './components/Projetos';
import { FAH } from './components/FAH';
import { Contato } from './components/Contato';

export default class App extends Component {
  static displayName = App.name;

  render () {
    return (
      <Layout>
            <Route exact path='/'     component={Home} />
            <Route path='/itinerario' component={Itinerario} />
            <Route path='/atuacao'    component={AtuacaoProfissional} />
            <Route path='/projetos'   component={Projetos} />
            <Route path='/fah' component={FAH} />
            <Route path='/contact' component={Contato} />
      </Layout>
    );
  }
}
