﻿import React, { Component } from 'react';
import { ListaComponente } from './ListaComponente';

export class MateriaCard extends Component {
    static displayName = MateriaCard.name;

    constructor(props) {
        super(props)
    }

    render() {
        return (
            <div class="card">

                <header class="card-header">
                    <div class="card-image">
                        <img src={this.props.img} />
                    </div>
                    <h1 class="card-text">{this.props.titulo}</h1>
                </header>

                <main class="card-conteudo">
                    <h6>Conteúdo</h6>
                    <ListaComponente data={this.props.conteudo} />

                    <h6>Pontos Positivos</h6>
                    <ListaComponente data={this.props.pontosNegativos} />

                    <h6>Pontos Negativos</h6>
                    <ListaComponente data={this.props.pontosPositivos} />
                </main>

                <footer>
                </footer>
            </div>
        );
    }
}
