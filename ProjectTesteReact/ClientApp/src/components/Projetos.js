﻿import React, { Component } from 'react';
import { PageTitle } from './PageTitle';

export class Projetos extends Component {
  
    render() {
        return (
            <main>
                <PageTitle title="Projetos" description="Projetos mais relevantes que já fiz" />
            </main>
        );
    }
}
