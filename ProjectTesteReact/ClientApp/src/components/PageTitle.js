﻿import React, { Component } from 'react';



export class PageTitle extends Component {
    static displayName = PageTitle.name;

    render() {
        return (
            <header class="header-page">
                <h1 class="title-page">{this.props.titulo}</h1>
                <h6 class="description-page">{this.props.descricao}</h6>
            </header>
        )
    }
}