﻿import React, { Component } from 'react';
import { PageTitle } from './PageTitle';
import ps4 from '../imgs/ps4.png';
import spfc from '../imgs/spfc.png';
import youtube from '../imgs/youtube.png';
import filme from '../imgs/filme.png';

export class FAH extends Component {

    render() {
        return (
            <main>
                <PageTitle title="Família/Amigos/Hobby" description="Projetos mais relevantes que já fiz" />

                <h4 class="header-section">Vídeo Game</h4>
                <section class="fah-section">
                    <div>
                        <img src={ps4} class="img-fah"/>
                    </div>
                    <div class="fah-content">
                        O PlayStation 4 (プレイステーション4 Pureisutēshon Fō?, oficialmente abreviada como PS4) é uma consola de videojogos, da oitava geração com arquitetura x86, produzida pela empresa Sony Interactive Entertainment e lançado em Novembro de 2013, como a quarta edição da série PlayStation, sucessora da PlayStation 3, competindo directamente com a Wii U da Nintendo e, com a Xbox One da Microsoft.
                    </div>
                </section>

                <h4 class="header-section">Futebol</h4>
                <section class="fah-section">
                    <div>
                        <img src={spfc} class="img-fah"/>
                    </div>
                    <div class="fah-content">
                        São Paulo Futebol Clube é um clube poliesportivo brasileiro da cidade de São Paulo, capital do estado homônimo. Foi fundado em 25 de janeiro de 1930,[2] tendo interrompido suas atividades em maio de 1935, e as retomado em dezembro do mesmo ano.[6] No futebol, é um dos clubes mais bem sucedidos do Brasil, sendo que, dentre seus principais títulos, destacam-se três Mundiais (recorde absoluto a nível nacional), três Copas Libertadores (recorde nacional compartilhado com Palmeiras, Santos e Grêmio), uma Copa Sul-Americana e seis Campeonatos Brasileiros.[7] Quanto a títulos internacionais, o São Paulo, com 12 conquistas, é o terceiro clube da América do Sul com o maior número de troféus, ficando atrás somente de Boca Juniors e Independiente.[8] No que diz respeito ao somatório de títulos oficiais de abrangência nacional e internacional de clubes brasileiros, em janeiro de 2021, o São Paulo figurava como o maior campeão do Brasil, com dezoito conquistas.
                    </div>
                </section>

                <h4 class="header-section">Youtube</h4>
                <section class="fah-section">
                    <div>
                        <img src={youtube} class="img-fah"/>
                    </div>
                    <div class="fah-content">
                        YouTube é uma plataforma de compartilhamento de vídeos com sede em San Bruno, Califórnia. O serviço foi criado por três ex-funcionários do PayPal - Chad Hurley, Steve Chen e Jawed Karim - em fevereiro de 2005. A Google comprou o site em novembro de 2006 por US$ 1,65 bilhão; desde então o YouTube funciona como uma das subsidiárias da Google The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).
                    </div>
                </section>

                <h4 class="header-section">Filmes</h4>
                <section class="fah-section">
                    <div>
                        <img src={filme} class="img-fah"/>
                    </div>
                    <div class="fah-content">
                        Filme, fita ou película é um produto audiovisual finalizado, com uma certa duração, para ser exibido no cinema, na televisão ou em algum outro veículo. Um filme é formado por uma série finita de imagens fixas, registradas sobre um suporte físico e que, projetadas a uma velocidade maior que a capacidade resolutiva da visão humana, dão ao espectador a sensação de movimento.
                    </div>
                </section>
            </main>
        );
    }
}
