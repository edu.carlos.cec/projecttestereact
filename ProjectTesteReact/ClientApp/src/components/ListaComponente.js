﻿import React, { Component } from 'react';

export class ListaComponente extends Component {
    static displayName = ListaComponente.name;

    render() {
        return (
            <ul>
                {this.props.data}
            </ul>
        );
    }
}