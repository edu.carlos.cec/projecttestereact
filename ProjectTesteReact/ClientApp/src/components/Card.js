﻿import React, { Component } from 'react';
import  image  from '../imgs/duratex.jpg';

export class Card extends Component {
    static displayName = Card.name;

    render() {
        return (
            <div class="card">

                <header class="card-header">
                    <div class="card-image">
                        <img src={this.props.image} />
                    </div>
                    <h1>{this.props.titulo}</h1>
                </header>

                <main>
                    <p class="card-text profissional">{ this.props.description }</p>
                </main>

                <footer class="card-footer">
                    <p>{this.props.footer}</p>
                </footer>
            </div>
        );
    }
}
