﻿import React, { Component } from 'react';
import { Card } from './Card';
import { PageTitle } from './PageTitle';
import DuratexImagem from '../imgs/duratex.png';
import GirolandoImagem from '../imgs/girolando.png';

export class AtuacaoProfissional extends Component {
    static displayName = AtuacaoProfissional.name;

    render() {
        return (
            <main>
                <PageTitle titulo="Atuação Profissional" descricao="Lugares que já trabalhei" />
                <div class="main-cards">
                    <Card image={DuratexImagem} titulo="Duratex" description="Duratex é uma empresa brasileira de capital aberto, com ações negociadas na Bolsa de Valores de São Paulo. Foi fundada em 1951 e é controlada pela Itaúsa que possui 40% das ações da Duratex e o Grupo Ligna com 20% das ações, o restante está na Bolsa de Valores de São Paulo." footer="Período: 11/2019 - 03/2021"/>
                    <Card image={GirolandoImagem} titulo="Girolando" description="A Associação Brasileira dos Criadores de Girolando foi fundada em 20 de dezembro de 1978, em Uberaba/MG, por pecuaristas da região que uniram forças em busca da defesa dos direitos da classe em âmbito nacional e também de meios para garantir a evolução genética dos rebanhos Girolando. A diretoria eleita provisoriamente, durante a assembleia ocorrida nessa data para criação da Associação dos Criadores de Gado de Leite do Triângulo Mineiro e Alto Paranaíba (Assoleite), tinha como presidente o pecuarista José Facuri Dib. " footer="Período: 17/2021 - Atual"/>
                </div>
             </main>
        );
    }
}
