﻿import React, { Component } from 'react';
import { PageTitle } from './PageTitle';


export class Contato extends Component {
    static displayName = Contato.name;

    constructor(props) {
        super(props)
        this.state = {
            nome: null,
            email: null,
            mensagem: null,
        }  
        this.mudarNome     = this.mudarNome.bind(this);
        this.mudarEmail    = this.mudarEmail.bind(this);
        this.mudarMensagem = this.mudarMensagem.bind(this);
        this.enviar        = this.enviar.bind(this);
    }

    mudarNome(e) {
        this.setState({ nome: e.target.value });
        console.log(this.state.nome)
    }

    mudarEmail(e) {
        this.setState({ email: e.target.value });
        console.log(this.state.email)
    }

    mudarMensagem(e) {
        this.setState({ mensagem: e.target.value });
        console.log(this.state.mensagem)
    }

    

    render() {
        return (
            <main >
                <PageTitle titulo="Contato" descricao="Entre em contato comigo" />

                <section class="section-contato">
                    <div class="form-contato">

                        <div class="form-input">
                            <label for="fname">Nome:</label><br/>
                            <input type="text" value={ this.state.nome } id="nome" name="nome" onChange={this.mudarNome}/><br />
                         </div>

                        <div class="form-input">
                            <label for="email">Email:</label><br />
                            <input type="text" value={this.state.email} id="email" name="email" onChange={this.mudarEmail} /><br />
                        </div>

                        <div class="form-input">
                            <label for="mensagem">Mensagem:</label><br />
                            <textarea type="text" value={this.state.mensagem} id="mensagem" name="mensagem" onChange={this.mudarMensagem}></textarea>
                        </div>
                        

                        <div class="form-divider">
                            <button id="btn_enviar" onClick={this.enviar}>Enviar</button>
                        </div>
                    </div>
                </section>
            </main>
        );
    }


    enviar() {

        if (
            (String(this.state.nome).trim() != "" && this.state.nome != null) &&
            (String(this.state.email).trim() != "" && this.state.email != null) &&
            (String(this.state.mensagem).trim() != "" && this.state.mensagem != null)
        ) {
            const requestOptions = {
                method: 'POST',
                headers: { 'Content-Type': 'application/json' },
                body: JSON.stringify(
                    {
                        nome: this.state.nome,
                        email: this.state.email,
                        mensagem: this.state.mensagem,
                    }
                )
            };

            fetch('http://localhost:8000/api/contato', requestOptions)
                .then(response => alert("Salvo!!"))
        } else {
            alert("Campos vazios, digite algo!!")
        }

    }



    
}
