import React, { Component } from 'react';
import { Card } from './Card';
import DuratexImagem from '../imgs/duratex.png';
import GirolandoImagem from '../imgs/girolando.png';
import IftmImagem from '../imgs/iftm.jpg';
import FotoPerfil from '../imgs/foto-perfil.jfif';

export class Home extends Component {
  static displayName = Home.name;

  render () {
    return (
        <main>

            <header class="person-header">
                <div class="person-image">
                    <img src={FotoPerfil} class="perfil-image" />
                </div>
                <h1 class="person-name">Carlos Eduardo Cabral</h1>
            </header>

            <h3 class="header-topic">Um pouco sobre mim: </h3>
            <p>Sou Carlos, tenho 21 anos, sou natural de Uberaba/MG, atualmente trabalho na empresa Girolando, uma instuição que cuida
            do registro e melhoramento da raça leiteira Girolando.
            Comecei minha graduação em Análise e Desenvolvimento de Sistemas em 2019, tenho previsão de concluir em 2022.
            Aqui nesse site você pode conhecer mais sobre mim, o que eu gosto, onde trabalhei, onde estudei e coisas mais pessoais.</p>

            <h3 class="header-topic">Atuação Acadêmica e Profissional: </h3>

            <h4 class="header-section">Profissional</h4>
            <section class="main-cards">
                <Card titulo="Duratex" image={DuratexImagem} description="Duratex é uma empresa brasileira de capital aberto, com ações negociadas na Bolsa de Valores de São Paulo. Foi fundada em 1951 e é controlada pela Itaúsa que possui 40% das ações da Duratex e o Grupo Ligna com 20% das ações, o restante está na Bolsa de Valores de São Paulo." footer="Período: 11/2019 - 03/2021"/>
                <Card titulo="Girolando" image={GirolandoImagem} description="A Associação Brasileira dos Criadores de Girolando foi fundada em 20 de dezembro de 1978, em Uberaba/MG, por pecuaristas da região que uniram forças em busca da defesa dos direitos da classe em âmbito nacional e também de meios para garantir a evolução genética dos rebanhos Girolando. A diretoria eleita provisoriamente, durante a assembleia ocorrida nessa data para criação da Associação dos Criadores de Gado de Leite do Triângulo Mineiro e Alto Paranaíba (Assoleite), tinha como presidente o pecuarista José Facuri Dib. " footer="Período: 17/2021 - Atual"/>
            </section>

            <h4 class="header-section">Acadêmica</h4>
            <section class="main-cards">
                <Card titulo="IFTM" image={IftmImagem} description="O Instituto Federal do Triângulo Mineiro (IFTM) é uma instituição pública de ensino, brasileira. Oferece educação básica, profissional e superior, de forma pluricurricular, localizada no Triângulo Mineiro. É uma instituição multicampi, especializada na oferta de educação profissional e tecnológica nas diferentes modalidades de ensino, com base na conjugação de conhecimentos técnicos e tecnológicos às suas práticas pedagógicas." footer="Período: 01/2019 - Atual"/>
            </section>

        </main>
    );
  }
}
