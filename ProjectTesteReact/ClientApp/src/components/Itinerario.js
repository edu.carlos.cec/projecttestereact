﻿import React, { Component } from 'react';
import { Card } from './Card';
import { MateriaCard } from './MateriaCard';
import { PageTitle } from './PageTitle';
import AlgoritimoImagem from '../imgs/algoritmo.jpg';
import OrientacaoAObjetos from '../imgs/OrientacaoAObjetos.png';
import Daw2 from '../imgs/daw2.png';


export class Itinerario extends Component {
    static displayName = Itinerario.name;

    constructor(props) {
        super(props)
        this.state = {
            dataAlgoritmo: [<li>Estrutura Sequencial</li>, <li>Condicional</li>, <li>Laço de Repetição</li>, <li>Vetor</li>, <li>Matriz</li>],
            dataAlgoritmoPontosPositivos: [<li>Conteúdo bem explicado</li>, <li>Bons exercícios</li>, <li>Tempo certo para cada matéria</li>],
            dataAlgoritmoPontosNegativos: [<li>Matéria se resolve muito tarde</li>],


            dataOrientaçãoAObjetos: [<li>Introdução</li>, <li>Objeto</li>, <li>Herança</li>, <li>Interface</li>, <li>Polimorfismo</li>],
            dataOrientaçãoAObjetosPontosPositivos: [<li>Muito Exercício</li>, <li>Professor explica bem</li>, <li>Trabalho Final Interessante</li>],
            dataOrientaçãoAObjetosPontosNegativos: [<li>Pouca explicação sobre JAVA</li>],


            dataDaw2: [<li>Introdução</li>, <li>Rotas</li>, <li>Controladores</li>, <li>JAVA SPRING</li>, <li>Banco de Dados</li>],
            dataDaw2PontosPositivos: [<li>Padrã MVC é muito usado</li>, <li>Boa didática</li>],
            dataDaw2PontosNegativos: [<li>Aulas muito longas</li>],
        
        }
    }

    render() {
        return (

            <main>
                <PageTitle titulo="Itinerário Formativo" descricao="As disciplinas que cursei na graduação" />

                <h4 class="header-section">Matérias de Desenvolvimento</h4>
                <section class="main-cards">
                    <MateriaCard img={AlgoritimoImagem} titulo="Algoritmo" conteudo={this.state.dataAlgoritmo} pontosNegativos={this.state.dataAlgoritmoPontosNegativos} pontosPositivos={this.state.dataAlgoritmoPontosPositivos} />
                    <MateriaCard img={OrientacaoAObjetos} titulo="Orientação a Objetos" conteudo={this.state.dataOrientaçãoAObjetos} pontosNegativos={this.state.dataOrientaçãoAObjetosPontosNegativos} pontosPositivos={this.state.dataOrientaçãoAObjetosPontosPositivos} />
                    <MateriaCard img={Daw2} titulo="Daw2" conteudo={this.state.dataDaw2} pontosNegativos={this.state.dataDaw2PontosNegativos} pontosPositivos={this.state.dataDaw2PontosPositivos} />
                </section>


            </main>
            
        );
    }
}